namespace Blackboard
{
    public static class BlackboardKeys
    {
        public const string UNIT = "Unit";
        public const string BARN = "Barn";
        
        
        public const string MOVE_POSITION = "MovePosition";
        public const string STOPPING_DISTANCE = "StoppingDistance";

        public const string NEAREST_TREE = "NearestTree";
    }
}