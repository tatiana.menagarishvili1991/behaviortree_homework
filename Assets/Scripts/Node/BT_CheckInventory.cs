﻿using Blackboard;
using Sample;
using UnityEngine;

namespace Lessons.AI.LessonBehaviourTree
{
    public sealed class BT_CheckInventory : BehaviourNode
    {
        [SerializeField]
        private HierarchicalStateMachine.Blackboard blackboard;

        [SerializeField] private bool _targetIsResourceBagFull = true;

        protected override void Run()
        {
             if (!this.blackboard.TryGetVariable(BlackboardKeys.UNIT, out Character unit))
             {
                 this.Return(false);
                 return;
             }

             if (unit.IsResourceBagFull() == _targetIsResourceBagFull)
             {
                 this.Return(true);
                 return;
             }
             
             this.Return(false);
        }
    }
}