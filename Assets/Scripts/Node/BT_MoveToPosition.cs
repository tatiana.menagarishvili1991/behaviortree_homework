﻿using Blackboard;
using Sample;
using UnityEngine;

namespace Lessons.AI.LessonBehaviourTree
{
    public sealed class BT_MoveToPosition : BehaviourNode
    {
        [SerializeField] private HierarchicalStateMachine.Blackboard blackboard;

        protected override void Run()
        {
            if (!this.blackboard.TryGetVariable(BlackboardKeys.UNIT, out Character unit))
            {
                this.Return(false);
                return;
            }

            if (!this.blackboard.TryGetVariable(BlackboardKeys.MOVE_POSITION, out Vector3 targetPoint))
            {
                this.Return(false);
                return;
            }

            MoveToPosition(unit);
        }

        private void MoveToPosition(Character unit)
        {
            var stoppingDistance = this.blackboard.GetVariable<float>(BlackboardKeys.STOPPING_DISTANCE);
            var targetPosition = this.blackboard.GetVariable<Vector3>(BlackboardKeys.MOVE_POSITION);

            var distanceVector = targetPosition - unit.transform.position;
            var distance = distanceVector.magnitude;
            if (distance <= stoppingDistance)
            {
                this.Return(true);
                return;
            }

            var direction = distanceVector.normalized;
            unit.Move(direction);


            this.Return(false);
        }
    }
}