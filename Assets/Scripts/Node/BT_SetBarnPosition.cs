﻿using Blackboard;
using Sample;
using UnityEngine;

namespace Lessons.AI.LessonBehaviourTree
{
    public sealed class BT_SetBarnPosition : BehaviourNode
    {
        [SerializeField]
        private HierarchicalStateMachine.Blackboard blackboard;
        
        protected override void Run()
        {
            if (!this.blackboard.TryGetVariable(BlackboardKeys.BARN, out Barn barn))
            {
                this.Return(false);
                return;
            }

            Vector3 targetPosition = barn.transform.position;
            this.blackboard.SetVariable(BlackboardKeys.MOVE_POSITION, targetPosition);
            this.Return(true);
        }
    }
}