﻿using Blackboard;
using Lessons.AI.LessonBehaviourTree;
using Sample;
using UnityEngine;
using Tree = Sample.Tree;

namespace Node
{
    public sealed class BT_Gather : BehaviourNode
    {
        [SerializeField] private Lessons.AI.HierarchicalStateMachine.Blackboard blackboard;

        protected override void Run()
        {
            if (!this.blackboard.TryGetVariable(BlackboardKeys.UNIT, out Character unit))
            {
                this.Return(false);
                return;
            }

            if (!this.blackboard.TryGetVariable(BlackboardKeys.NEAREST_TREE, out Tree tree))
            {
                this.Return(false);
                return;
            }

            if (unit.IsChopping)
            {
                this.Return(true);
                return;
            }

            if (!tree.HasResources())
            {
                this.Return(false);
                return;
            }


            unit.Chop(tree);
            this.Return(true);
        }
    }
}