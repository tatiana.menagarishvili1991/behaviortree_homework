﻿using Blackboard;
using Sample;
using UnityEngine;
using Tree = Sample.Tree;

namespace Lessons.AI.LessonBehaviourTree
{
    public sealed class BT_SetTreePosition : BehaviourNode
    {
        [SerializeField]
        private HierarchicalStateMachine.Blackboard blackboard;


        protected override void Run()
        {
             if (!this.blackboard.TryGetVariable(BlackboardKeys.UNIT, out Character unit))
             {
                 this.Return(false);
                 return;
             }

             GetNearestTree(unit.transform.position);
        }

        private void GetNearestTree(Vector3 unitPosition)
        {
            if (blackboard.TryGetVariable(BlackboardKeys.NEAREST_TREE, out Tree nearestTree))
            {
                this.blackboard.SetVariable(BlackboardKeys.MOVE_POSITION, nearestTree.transform.position);
                this.Return(true);
            }
            
            this.Return(false);
        }
    }
}