using System.Collections.Generic;
using System.Linq;
using Blackboard;
using UnityEngine;

namespace Sample.Sensors
{
    public class TreeSensor : MonoBehaviour
    {
        [SerializeField] private Lessons.AI.HierarchicalStateMachine.Blackboard _blackboard;
        [SerializeField] private Transform _centerPoint;
        [SerializeField] private float _updatePeriod;
        private float _timer;

        private List<Tree> _trees;

        private void Awake()
        {
            _trees = FindObjectsOfType<Tree>().ToList();
        }

        public void FixedUpdate()
        {
            _timer += Time.fixedDeltaTime;
            if (_timer >= _updatePeriod)
            {
                _timer = 0;
                UpdateTrees();
            }
        }

        private void UpdateTrees()
        {
            _trees.Sort((tree1, tree2) =>
            {
                var sqrMagnitudeToTree1 = SqrMagnitudeTo(tree1);
                var sqrMagnitudeToTree2 = SqrMagnitudeTo(tree2);
                
                if (sqrMagnitudeToTree1 > sqrMagnitudeToTree2)
                {
                    return 1;
                }
                if (sqrMagnitudeToTree1 < sqrMagnitudeToTree2)
                {
                    return -1;
                }
                return 0;
            });

            var nearestTree = _trees.FirstOrDefault(tree => tree.HasResources());
            if (nearestTree != null)
            {
                _blackboard.SetVariable(BlackboardKeys.NEAREST_TREE, nearestTree);
            }
            else
            {
                _blackboard.RemoveVariable(BlackboardKeys.NEAREST_TREE);
            }
            
        }

        private float SqrMagnitudeTo(Tree tree)
        {
            return Vector3.SqrMagnitude(tree.transform.position - _centerPoint.position);
        }
    }
}